# **********************  RESOURCE GROUPS ********************** #

resource "azurerm_resource_group" "compute" {
  name     = "AS-${var.azureenvironment}RESOURCES"
  location = "${var.location}"
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}

resource "azurerm_resource_group" "network" {
  name     = "AS-${var.azureenvironment}NETWORKRESOURCES"
  location = "${var.location}"
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}
