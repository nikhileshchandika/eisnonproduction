# **********************  SUBNETS ********************** #

resource "azurerm_subnet" "app" {
  name                      = "${var.azureenvironment}APP-SUBNET"
  virtual_network_name      = "${azurerm_virtual_network.vnet.name}"
  resource_group_name       = "${azurerm_resource_group.network.name}"
  address_prefix            = "${var.appsubnet_prefix}"
  network_security_group_id = "${azurerm_network_security_group.app.id}"
  route_table_id            = "${azurerm_route_table.routetable.id}"
  service_endpoints         = ["Microsoft.Storage"]
  depends_on                = ["azurerm_virtual_network.vnet", "azurerm_virtual_network_peering.vnetpeersb", "azurerm_virtual_network_peering.remotevnetpeersb", "azurerm_virtual_network_peering.vnetpeernb", "azurerm_virtual_network_peering.remotevnetpeernb"]
}

resource "azurerm_subnet" "dxp" {
  name                 = "${var.azureenvironment}DXP-SUBNET"
  virtual_network_name = "${azurerm_virtual_network.vnet.name}"
  resource_group_name  = "${azurerm_resource_group.network.name}"
  address_prefix       = "${var.dxpsubnet_prefix}"
  network_security_group_id = "${azurerm_network_security_group.dxp.id}"
  route_table_id       = "${azurerm_route_table.routetable.id}"
  service_endpoints         = ["Microsoft.Storage"]
  depends_on           = ["azurerm_virtual_network.vnet", "azurerm_virtual_network_peering.vnetpeersb", "azurerm_virtual_network_peering.remotevnetpeersb", "azurerm_virtual_network_peering.vnetpeernb", "azurerm_virtual_network_peering.remotevnetpeernb"]
}

resource "azurerm_subnet" "lb" {
  name                 = "${var.azureenvironment}LB-SUBNET"
  virtual_network_name = "${azurerm_virtual_network.vnet.name}"
  resource_group_name  = "${azurerm_resource_group.network.name}"
  address_prefix       = "${var.lbsubnet_prefix}"
  network_security_group_id = "${azurerm_network_security_group.lb.id}"
  route_table_id      = "${azurerm_route_table.routetable.id}"
  service_endpoints         = ["Microsoft.Storage"]
  depends_on           = ["azurerm_virtual_network.vnet", "azurerm_virtual_network_peering.vnetpeersb", "azurerm_virtual_network_peering.remotevnetpeersb", "azurerm_virtual_network_peering.vnetpeernb", "azurerm_virtual_network_peering.remotevnetpeernb"]
}
