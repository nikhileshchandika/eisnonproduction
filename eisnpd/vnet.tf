# **********************  VNET / SUBNETS ********************** #
resource "azurerm_virtual_network" "vnet" {
  name                = "${var.azureenvironment}-VNET"
  resource_group_name = "${azurerm_resource_group.network.name}"
  location            = "${azurerm_resource_group.network.location}"
  address_space       = ["${var.vnet_prefix}"]
  dns_servers         = ["${var.dns01}", "${var.dns02}"]
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}


resource "azurerm_virtual_network_peering" "vnetpeersb" {
  name                         = "${var.azureenvironment}-VNET-TO-${var.cloudguardvnetsb_name}"
  resource_group_name          = "${azurerm_resource_group.network.name}"
  virtual_network_name         = "${azurerm_virtual_network.vnet.name}"
  remote_virtual_network_id    = "${var.cloudguardvnetsb_id}"
  allow_virtual_network_access = true
  allow_forwarded_traffic      = true
  allow_gateway_transit        = false
}

resource "azurerm_virtual_network_peering" "remotevnetpeersb" {
  provider                     = "azurerm.gateway"
  name                         = "${var.cloudguardvnetsb_name}-TO-${var.azureenvironment}-VNET"
  resource_group_name          = "${var.remote_resourcegroupsb_name}"
  virtual_network_name         = "${var.cloudguardvnetsb_name}"
  remote_virtual_network_id    = "${azurerm_virtual_network.vnet.id}"
  allow_virtual_network_access = true
  allow_forwarded_traffic      = true
  allow_gateway_transit        = true
}

resource "azurerm_virtual_network_peering" "vnetpeernb" {
  name                         = "${var.azureenvironment}-VNET-TO-${var.cloudguardvnetnb_name}"
  resource_group_name          = "${azurerm_resource_group.network.name}"
  virtual_network_name         = "${azurerm_virtual_network.vnet.name}"
  remote_virtual_network_id    = "${var.cloudguardvnetnb_id}"
  allow_virtual_network_access = true
  allow_forwarded_traffic      = true
  allow_gateway_transit        = false
}

resource "azurerm_virtual_network_peering" "remotevnetpeernb" {
  provider                     = "azurerm.gateway"
  name                         = "${var.cloudguardvnetnb_name}-TO-${var.azureenvironment}-VNET"
  resource_group_name          = "${var.remote_resourcegroupnb_name}"
  virtual_network_name         = "${var.cloudguardvnetnb_name}"
  remote_virtual_network_id    = "${azurerm_virtual_network.vnet.id}"
  allow_virtual_network_access = true
  allow_forwarded_traffic      = true
  allow_gateway_transit        = true
}

resource "azurerm_virtual_network_peering" "vnetpeersql" {
  name                         = "${var.azureenvironment}-VNET-TO-${var.sqlvnet_name}"
  resource_group_name          = "${azurerm_resource_group.network.name}"
  virtual_network_name         = "${azurerm_virtual_network.vnet.name}"
  remote_virtual_network_id    = "${var.sqlvnet_id}"
  allow_virtual_network_access = true
  allow_forwarded_traffic      = true
  allow_gateway_transit        = false
}

resource "azurerm_virtual_network_peering" "remotevnetpeersql" {
  name                         = "${var.sqlvnet_name}-TO-${var.azureenvironment}-VNET"
  resource_group_name          = "${var.remote_sqlvnet_name}"
  virtual_network_name         = "${var.sqlvnet_name}"
  remote_virtual_network_id    = "${azurerm_virtual_network.vnet.id}"
  allow_virtual_network_access = true
  allow_forwarded_traffic      = true
  allow_gateway_transit        = false
}

