# **********************  DNS RECORDS ********************** #

resource "azurerm_dns_a_record" "adm" {
  provider            = "azurerm.gateway"
  name                = "${var.adm_record_name}"
  zone_name           = "${var.dns_zone_name}"
  resource_group_name = "${var.dns_zone_resource_group}"
  ttl                 = 300
  records             = ["${var.admlb_ip}"]
  
}

resource "azurerm_dns_a_record" "app" {
  provider            = "azurerm.gateway"
  name                = "${var.app_record_name}"
  zone_name           = "${var.dns_zone_name}"
  resource_group_name = "${var.dns_zone_resource_group}"
  ttl                 = 300
  records             = ["${var.applb_ip}"]
}

resource "azurerm_dns_a_record" "rat" {
  provider            = "azurerm.gateway"
  name                = "${var.rat_record_name}"
  zone_name           = "${var.dns_zone_name}"
  resource_group_name = "${var.dns_zone_resource_group}"
  ttl                 = 300
  records             = ["${var.ratlb_ip}"]
}

resource "azurerm_dns_a_record" "sol" {
  provider            = "azurerm.gateway"
  name                = "${var.sol_record_name}"
  zone_name           = "${var.dns_zone_name}"
  resource_group_name = "${var.dns_zone_resource_group}"
  ttl                 = 300
  records             = ["${var.sollb_ip}"]
}


resource "azurerm_dns_a_record" "dxp" {
  provider            = "azurerm.gateway"
  name                = "${var.dxp_record_name}"
  zone_name           = "${var.dns_zone_name}"
  resource_group_name = "${var.dns_zone_resource_group}"
  ttl                 = 300
  records             = ["${var.dxplb_ip}"]
}