# **********************  PROVIDERS ********************** #

variable "AZURE_SUBSCRIPTION_ID" {
 default     = "75eb7e46-e30a-40ee-a902-88f84b3cdcfc"
}

variable "AZURE_SUBSCRIPTION_TOWERA_ID" {
 default     = "0e9ae085-52a3-461c-a558-09a9e45da255"
}

variable "AZURE_CLIENT_ID" {
 default     = "86a5fdb6-7fa7-4ffb-8368-bdc45e9fd0c5"
}

variable "AZURE_CLIENT_SECRET" {
 default     = "69PkUPWXy4d2R4ONflvPsJMXoquWiFidd/hHyNwtHYQ="
}

variable "AZURE_TENANT_ID" {
 default     = "ff85220d-ac35-48f0-b12a-0ca11f170147"
}

variable "VM_ADMIN" {
 default     = ".cmmjackson"
}
# **********************  RESOURCE GROUPS ********************** #

variable "environment" {}

variable "azureenvironment" {}


# **********************  LOCATION ********************** #

variable "location" {}


# **********************  VNET ********************** #


variable "vnet_prefix" {}

variable "cloudguardvnetsb_id" {
default     = "/subscriptions/0e9ae085-52a3-461c-a558-09a9e45da255/resourceGroups/AS-CLOUDGUARDNORTHBOUNDRESOURCES/providers/Microsoft.Network/virtualNetworks/INFSCGNPRD-VNET"
}

variable "cloudguardvnetsb_name" {
default     = "INFSCGSPRD-VNET"
}

variable "remote_resourcegroupsb_name" {
 default     = "AS-CLOUDGUARDSOUTHBOUNDRESOURCES"
}

variable "cloudguardvnetnb_id" {
default     = "/subscriptions/0e9ae085-52a3-461c-a558-09a9e45da255/resourceGroups/AS-CLOUDGUARDSOUTHBOUNDRESOURCES/providers/Microsoft.Network/virtualNetworks/INFSCGSPRD-VNET"
}

variable "cloudguardvnetnb_name" {
default     = "INFSCGNPRD-VNET"
}

variable "remote_resourcegroupnb_name" {
 default     = "AS-CLOUDGUARDNORTHBOUNDRESOURCES"
}

variable "sqlvnet_id" {}

variable "sqlvnet_name" {}

variable "remote_sqlvnet_name" {}

#**********************  DNS ********************** #

variable "dns_zone_name" {
default     = "towercloud.co.nz"
}

variable "dns_zone_resource_group" {
default     = "AS-DNSRESOURCES"
}

variable "dns01" {}

variable "dns02" {}

variable "adm_record_name" {}

variable "app_record_name" {}

variable "rat_record_name" {}

variable "sol_record_name" {}

variable "dxp_record_name" {}


# **********************  ROUTE TABLE ********************** #

variable "nexthopsb_ip" {
default     = "100.64.167.164"
}

variable "nexthopnb_ip" {
default     = "100.64.167.36"
}

# **********************  SUBNETS ********************** #

variable "appsubnet_prefix" {}

variable "dxpsubnet_prefix" {}

variable "lbsubnet_prefix" {}

# **********************  IP ADDRESSES ********************** #

variable "nic_adm_ip" {}

variable "nic_app_ip" {}

variable "nic_rat_ip" {}

variable "nic_sol_ip" {}

variable "nic_dxp_ip" {}

# **********************  LOAD BALANCERS ********************** #

variable "admlb_ip" {}

variable "applb_ip" {}

variable "ratlb_ip" {}

variable "sollb_ip" {}

variable "dxplb_ip" {}

# **********************  STORAGE ACCOUNTS ********************** #

variable "storage01_name" {}

variable "filestorage" {}

variable "filestoragepassword" {}

variable "storage_account_tier" {
default     = "Standard"
}

variable "storage_account_kind" {
default     = "StorageV2"
}

variable "storage_replication_type" {
default     = "RAGRS"
}

# **********************  VIRTUAL MACHINES ********************** #


variable "os_image_publisher" {
default     = "center-for-internet-security-inc"
}

variable "os_image_offer" {
default     = "cis-centos-7-v2-1-1-l1"
}

variable "os_sku" {
default     = "cis-centos7-l1"
}

variable "os_version" {
default     = "4.0.1"
}

# **********************  DOMAIN JOIN ********************** #

variable "VM_DOMAINUSER" {
default     = ".ztdadmin@nz.mytower.net"
}

variable "VM_DOMAIN" {
default     = "nz.mytower.net"
}

variable "VM_DOMAINOU" {}

variable "realm" {
default     = "NZ.MYTOWER.NET"
}

variable "vm_app_size" {
default     = "Standard_D4s_v3"
}

variable "vm_adm_size" {
default     = "Standard_D4s_v3"
}

variable "vm_sol_size" {
default     = "Standard_D2s_v3"
}

variable "vm_dxp_size" {
default     = "Standard_D2s_v3"
}

variable "vm_rat_size" {
default     = "Standard_D2s_v3"
}
# **********************  OTHERS ********************** #

variable "time" {}



# **********************  TAGS ********************** #

variable "app_tag_name" {}


