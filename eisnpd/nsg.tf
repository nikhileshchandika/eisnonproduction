# **********************  NETWORK SECURITY GROUPS ********************** #
resource "azurerm_network_security_group" "app" {
  name                = "${var.azureenvironment}APP-NSG"
  resource_group_name = "${azurerm_resource_group.network.name}"
  location            = "${azurerm_resource_group.network.location}"
  
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}

resource "azurerm_network_security_group" "dxp" {
  name                = "${var.azureenvironment}DXP-NSG"
  resource_group_name = "${azurerm_resource_group.network.name}"
  location            = "${azurerm_resource_group.network.location}"
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}

resource "azurerm_network_security_group" "lb" {
  name                = "${var.azureenvironment}LB-NSG"
  resource_group_name = "${azurerm_resource_group.network.name}"
  location            = "${azurerm_resource_group.network.location}"
  
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}

# **********************  NETWORK SECURITY Associations ********************** #


resource "azurerm_subnet_network_security_group_association" "app" {
  subnet_id                 = "${azurerm_subnet.app.id}"
  network_security_group_id = "${azurerm_network_security_group.app.id}"
}

resource "azurerm_subnet_network_security_group_association" "dxp" {
  subnet_id                 = "${azurerm_subnet.dxp.id}"
  network_security_group_id = "${azurerm_network_security_group.dxp.id}"
}

resource "azurerm_subnet_network_security_group_association" "lb" {
  subnet_id                 = "${azurerm_subnet.lb.id}"
  network_security_group_id = "${azurerm_network_security_group.lb.id}"
}

