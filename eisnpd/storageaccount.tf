# **********************  STORAGE ACCOUNTS ********************** #
resource "azurerm_storage_account" "storageaccount" {
  name                     = "${var.storage01_name}"
  resource_group_name      = "${azurerm_resource_group.compute.name}"
  location                 = "${azurerm_resource_group.compute.location}"
  account_tier             = "${var.storage_account_tier}"
  account_kind             = "${var.storage_account_kind}"
  account_replication_type = "${var.storage_replication_type}"
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}


