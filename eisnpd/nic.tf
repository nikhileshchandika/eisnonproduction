# **********************  NETWORK INTERFACE ********************** #

resource "azurerm_network_interface" "adm" {

  name                      = "${var.environment}ADM0${count.index+1}-NIC"
  location                  = "${azurerm_resource_group.network.location}"
  enable_accelerated_networking = "false"
  resource_group_name       = "${azurerm_resource_group.network.name}"
  depends_on                = ["azurerm_subnet.app", "azurerm_subnet.dxp"]
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = "${azurerm_subnet.app.id}"
    private_ip_address_allocation = "Static"
    private_ip_address            = "${var.nic_adm_ip}"
  }
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}

resource "azurerm_network_interface_backend_address_pool_association" "adm" {
  network_interface_id    = "${azurerm_network_interface.adm.id}"
  ip_configuration_name   = "${azurerm_network_interface.adm.ip_configuration.0.name}"
  backend_address_pool_id = "${azurerm_lb_backend_address_pool.adm.id}"
}
# **********************  NETWORK INTERFACE ********************** #

resource "azurerm_network_interface" "app" {

  name                      = "${var.environment}APP0${count.index+1}-NIC"
  location                  = "${azurerm_resource_group.network.location}"
  resource_group_name       = "${azurerm_resource_group.network.name}"
  enable_accelerated_networking = "false"
  depends_on                = ["azurerm_subnet.app", "azurerm_subnet.dxp"]
  
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = "${azurerm_subnet.app.id}"
    private_ip_address_allocation = "Static"
    private_ip_address            = "${var.nic_app_ip}"
  }
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}


resource "azurerm_network_interface_backend_address_pool_association" "app" {
  network_interface_id    = "${azurerm_network_interface.app.id}"
  ip_configuration_name   = "${azurerm_network_interface.app.ip_configuration.0.name}"
  backend_address_pool_id = "${azurerm_lb_backend_address_pool.app.id}"
}


# **********************  NETWORK INTERFACE ********************** #

resource "azurerm_network_interface" "rat" {

  name                      = "${var.environment}RAT0${count.index+1}-NIC"
  location                  = "${azurerm_resource_group.network.location}"
  resource_group_name       = "${azurerm_resource_group.network.name}"
  enable_accelerated_networking = "false"
  depends_on                = ["azurerm_subnet.app", "azurerm_subnet.dxp"]
  
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = "${azurerm_subnet.app.id}"
    private_ip_address_allocation = "Static"
    private_ip_address            = "${var.nic_rat_ip}"
  }
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}


resource "azurerm_network_interface_backend_address_pool_association" "rat" {
  network_interface_id    = "${azurerm_network_interface.rat.id}"
  ip_configuration_name   = "${azurerm_network_interface.rat.ip_configuration.0.name}"
  backend_address_pool_id = "${azurerm_lb_backend_address_pool.rat.id}"
}



# **********************  NETWORK INTERFACE ********************** #

resource "azurerm_network_interface" "sol" {

  name                      = "${var.environment}SOL0${count.index+1}-NIC"
  location                  = "${azurerm_resource_group.network.location}"
  resource_group_name       = "${azurerm_resource_group.network.name}"
  enable_accelerated_networking = "false"
  depends_on                = ["azurerm_subnet.app", "azurerm_subnet.dxp"]
  
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = "${azurerm_subnet.app.id}"
    private_ip_address_allocation = "Static"
    private_ip_address            = "${var.nic_sol_ip}"

  }
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}

resource "azurerm_network_interface_backend_address_pool_association" "sol" {
  network_interface_id    = "${azurerm_network_interface.sol.id}"
  ip_configuration_name   = "${azurerm_network_interface.sol.ip_configuration.0.name}"
  backend_address_pool_id = "${azurerm_lb_backend_address_pool.sol.id}"
}

# **********************  NETWORK INTERFACE ********************** #

resource "azurerm_network_interface" "dxp" {

  name                      = "${var.environment}DXP0${count.index+1}-NIC"
  location                  = "${azurerm_resource_group.network.location}"
  resource_group_name       = "${azurerm_resource_group.network.name}"
  enable_accelerated_networking = "false"
  depends_on                = ["azurerm_subnet.app", "azurerm_subnet.dxp"]
  
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = "${azurerm_subnet.dxp.id}"
    private_ip_address_allocation = "Static"
    private_ip_address            = "${var.nic_dxp_ip}"
  }
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}


resource "azurerm_network_interface_backend_address_pool_association" "dxp" {
  network_interface_id    = "${azurerm_network_interface.dxp.id}"
  ip_configuration_name   = "${azurerm_network_interface.dxp.ip_configuration.0.name}"
  backend_address_pool_id = "${azurerm_lb_backend_address_pool.dxp.id}"
}

