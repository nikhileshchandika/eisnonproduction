# **********************  AVAILABILITY SET ********************** #
resource "azurerm_availability_set" "adm" {
  name                         = "${var.environment}ADM-AS"
  location                     = "${azurerm_resource_group.compute.location}"
  resource_group_name          = "${azurerm_resource_group.compute.name}"
  managed                      = true
  platform_update_domain_count = 5
  platform_fault_domain_count  = 2
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
} 

resource "azurerm_availability_set" "app" {
  name                         = "${var.environment}APP-AS"
  location                     = "${azurerm_resource_group.compute.location}"
  resource_group_name          = "${azurerm_resource_group.compute.name}"
  managed                      = true
  platform_update_domain_count = 5
  platform_fault_domain_count  = 2
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
} 

  resource "azurerm_availability_set" "rat" {
  name                         = "${var.environment}RAT-AS"
  location                     = "${azurerm_resource_group.compute.location}"
  resource_group_name          = "${azurerm_resource_group.compute.name}"
  managed                      = true
  platform_update_domain_count = 5
  platform_fault_domain_count  = 2
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
} 


  resource "azurerm_availability_set" "sol" {
  name                         = "${var.environment}SOL-AS"
  location                     = "${azurerm_resource_group.compute.location}"
  resource_group_name          = "${azurerm_resource_group.compute.name}"
  managed                      = true
  platform_update_domain_count = 5
  platform_fault_domain_count  = 2
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
} 


  resource "azurerm_availability_set" "dxp" {
  name                         = "${var.environment}DXP-AS"
  location                     = "${azurerm_resource_group.compute.location}"
  resource_group_name          = "${azurerm_resource_group.compute.name}"
  managed                      = true
  platform_update_domain_count = 5
  platform_fault_domain_count  = 2
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}

  

