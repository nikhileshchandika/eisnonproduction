# **********************  Route Table ********************** #

resource "azurerm_route_table" "routetable" {
  name                = "${var.azureenvironment}-ROUTETABLE"
  location            = "${azurerm_resource_group.network.location}"
  resource_group_name = "${azurerm_resource_group.network.name}"
  
  route {
    name                   = "${var.azureenvironment}SB-ROUTE"
    address_prefix         = "10.0.0.0/8"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthopsb_ip}"
  }
  
  route {
    name                   = "${var.azureenvironment}NB-ROUTE"
    address_prefix         = "0.0.0.0/0"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthopnb_ip}"
  }
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}

# **********************  Route Associations ********************** #

resource "azurerm_subnet_route_table_association" "app" {
  subnet_id      = "${azurerm_subnet.app.id}"
  route_table_id = "${azurerm_route_table.routetable.id}"
  depends_on     = ["azurerm_subnet.app", "azurerm_subnet.dxp"]
}

resource "azurerm_subnet_route_table_association" "dxp" {
  subnet_id      = "${azurerm_subnet.dxp.id}"
  route_table_id = "${azurerm_route_table.routetable.id}"
  depends_on     = ["azurerm_subnet.app", "azurerm_subnet.dxp"]
}

resource "azurerm_subnet_route_table_association" "lb" {
  subnet_id      = "${azurerm_subnet.lb.id}"
  route_table_id = "${azurerm_route_table.routetable.id}"
  depends_on     = ["azurerm_subnet.app", "azurerm_subnet.dxp"]
}