# ********************** EISsi2adm VIRTUAL MACHINE ********************** #

resource "azurerm_virtual_machine" "adm" {
  count                 = 1
  name                  = "${var.environment}ADM0${count.index+1}"
  resource_group_name   = "${azurerm_resource_group.compute.name}"
  location              = "${azurerm_resource_group.compute.location}"
  vm_size               = "${var.vm_adm_size}"
  network_interface_ids = ["${element(azurerm_network_interface.adm.*.id, count.index)}"]
  availability_set_id   = "${azurerm_availability_set.adm.id}"
  delete_os_disk_on_termination = "true"
  delete_data_disks_on_termination = "true"
  depends_on            = ["azurerm_storage_account.storageaccount", "azurerm_network_interface.adm", "azurerm_virtual_network_peering.vnetpeersb"]
  
  plan {
    name      = "${var.os_sku}"
    publisher = "${var.os_image_publisher}"
    product   = "${var.os_image_offer}"
  }

  storage_image_reference {
    publisher = "${var.os_image_publisher}"
    offer     = "${var.os_image_offer}"
    sku       = "${var.os_sku}"
    version   = "${var.os_version}"
  }
  
  storage_os_disk {
    name                = "${var.environment}ADM01-OSDISK"
    managed_disk_type   = "Standard_LRS"
    create_option       = "FromImage"
    caching             = "ReadWrite"
  }

  os_profile {
    computer_name  = "${var.environment}ADM0${count.index+1}"
	admin_username = "${var.VM_ADMIN}"
    admin_password = "${data.azurerm_key_vault_secret.os-adminpassword.value}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }
  
  boot_diagnostics {
  enabled     = true
  storage_uri = "${azurerm_storage_account.storageaccount.primary_blob_endpoint}"
  }

  provisioner "remote-exec" {
    inline = [
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S mkdir /opt/tomcat",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S mkdir /opt/ant",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S mkdir /opt/java",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S mkdir /opt/mnt",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S mkdir /opt/mnt/efolder",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S mkdir /opt/mnt/inbound",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S mkdir /opt/mnt/outbound",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S mkdir /opt/mnt/jams",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S yum -y install oddjob realmd samba samba-common oddjob-mkhomedir sssd adcli ntpdate ntp",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S yum -y install unzip",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo \" DOMAIN='nz.mytower.net towercloud.co.nz' \" | sudo tee --append /etc/sysconfig/network-scripts/ifcfg-eth0",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S systemctl restart network",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S systemctl stop iptables",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S systemctl disable iptables",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S timedatectl set-timezone Pacific/Auckland",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S systemctl enable chronyd.service",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S systemctl start chronyd.service",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S sed -i 's/server 0.centos.pool.ntp.org iburst/server '${var.time}'.towercloud.co.nz iburst/g' /etc/chrony.conf",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S sed -i 's/server 1.centos.pool.ntp.org iburst//g' /etc/chrony.conf",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S sed -i 's/server 2.centos.pool.ntp.org iburst//g' /etc/chrony.conf",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S sed -i 's/server 3.centos.pool.ntp.org iburst//g' /etc/chrony.conf",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S systemctl daemon-reload",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S systemctl restart chronyd.service",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S systemctl status chronyd.service",
    "wget https://productionrespository.blob.core.windows.net/terraformbuildscripts/apache-ant-1.8.4-bin.tar.gz -P /tmp",
    "wget https://productionrespository.blob.core.windows.net/terraformbuildscripts/apache-tomcat-8.5.34.tar.gz -P /tmp",
    "wget https://productionrespository.blob.core.windows.net/terraformbuildscripts/jdk-8u181-linux-x64.tar.gz -P /tmp",
    "wget https://productionrespository.blob.core.windows.net/terraformbuildscripts/jarfiles.zip -P /tmp",
	"echo '${data.azurerm_key_vault_secret.domainJoinpassword.value}' | sudo -S realm join --user='${var.VM_DOMAINUSER}' '${var.VM_DOMAIN}' --computer-ou='${var.VM_DOMAINOU}'",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S realm deny --realm '${var.realm}' --all",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S realm permit --realm '${var.realm}' --groups its.'${var.environment}'.ADM@nz.mytower.net",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S realm permit --realm '${var.realm}' --groups 'Domain Admins@nz.mytower.net'",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo '%its.'${var.environment}'.ADM@nz.mytower.net ALL=(ALL:ALL) ALL' | sudo tee --append /etc/sudoers.d/toweradmins",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo '%Domain\\ Admins@nz.mytower.net ALL=(ALL:ALL) ALL' | sudo tee --append /etc/sudoers.d/toweradmins",
    "tar xzf /tmp/apache-ant-1.8.4-bin.tar.gz -C /tmp",
    "tar xzf /tmp/apache-tomcat-8.5.34.tar.gz -C /tmp",
    "tar xzf /tmp/jdk-8u181-linux-x64.tar.gz -C /tmp",
    "unzip /tmp/jarfiles.zip -d /tmp",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S cp -R /tmp/apache-ant-1.8.4/* /opt/ant",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S cp -R /tmp/apache-tomcat-8.5.34/* /opt/tomcat",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S cp -R /tmp/jdk1.8.0_181/* /opt/java",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S cp -R /tmp/jarfiles/* /opt/tomcat/lib",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S useradd -c 'eis serviceaccount' -m eis",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | echo 'eis:'${data.azurerm_key_vault_secret.eisserviceaccount-password.value}'' | sudo -S chpasswd",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S usermod -aG wheel eis",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S rm -f /tmp/apache-ant-1.8.4-bin.tar.gz",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S rm -f /tmp/apache-tomcat-8.5.34.tar.gz",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S rm -f /tmp/jdk-8u181-linux-x64.tar.gz",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S rm -f /tmp/jarfiles.zip",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S rm -rf /tmp/apache-ant-1.8.4/",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S rm -rf /tmp/apache-tomcat-8.5.34/",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S rm -rf /tmp/jdk1.8.0_181/",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S rm -rf /tmp/jarfiles/",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S chown -R eis:eis /opt/java",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S chown -R eis:eis /opt/ant",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S chown -R eis:eis /opt/tomcat",
	"wget https://productionrespository.blob.core.windows.net/terraformbuildscripts/tomcat.service -P /tmp",
    "echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S cp /tmp/tomcat.service /etc/systemd/system/",
    "echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S systemctl daemon-reload",
    "echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S systemctl enable tomcat",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S chown -R eis:eis /opt/mnt/inbound",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S chown -R eis:eis /opt/mnt/outbound",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S chown -R eis:eis /opt/mnt/jams",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S chown -R eis:eis /opt/mnt/efolder",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S chown -R eis:eis /opt/mnt",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S rm -rf /tmp/tomcat.service/",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S mount -t cifs //'${var.filestorage}'.file.core.windows.net/efolder /opt/mnt/efolder -o vers=3.0,username='${var.filestorage}',password='${var.filestoragepassword}',dir_mode=0777,file_mode=0777,sec=ntlmssp",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S mount -t cifs //'${var.filestorage}'.file.core.windows.net/inbound /opt/mnt/inbound -o vers=3.0,username='${var.filestorage}',password='${var.filestoragepassword}',dir_mode=0777,file_mode=0777,sec=ntlmssp",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S mount -t cifs //'${var.filestorage}'.file.core.windows.net/jams /opt/mnt/jams -o vers=3.0,username='${var.filestorage}',password='${var.filestoragepassword}',dir_mode=0777,file_mode=0777,sec=ntlmssp",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S mount -t cifs //'${var.filestorage}'.file.core.windows.net/outbound /opt/mnt/outbound -o vers=3.0,username='${var.filestorage}',password='${var.filestoragepassword}',dir_mode=0777,file_mode=0777,sec=ntlmssp",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo 'export JAVA_HOME=/opt/java' | sudo tee --append /etc/profile.d/sh.local",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo 'export JRE_HOME=/opt/java/jre' | sudo tee --append /etc/profile.d/sh.local",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo 'export PATH=$PATH:/opt/java/bin:/opt/java/jre' | sudo tee --append /etc/profile.d/sh.local",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo 'export ANT_HOME=/opt/ant' | sudo tee --append /etc/profile.d/sh.local",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo 'export PATH=$PATH:$ANT_HOME/bin' | sudo tee --append /etc/profile.d/sh.local",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S mkdir /etc/smbcredentials",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S touch /etc/smbcredentials/storageaccount.cred",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo username='${var.filestorage}' | sudo tee --append /etc/smbcredentials/storageaccount.cred",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo password='${var.filestoragepassword}' | sudo tee --append /etc/smbcredentials/storageaccount.cred",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S chmod 600 /etc/smbcredentials/storageaccount.cred",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo '//'${var.filestorage}'.file.core.windows.net/efolder /opt/mnt/efolder cifs nofail,vers=3.0,credentials=/etc/smbcredentials/storageaccount.cred,dir_mode=0777,file_mode=0777,serverino' | sudo tee --append /etc/fstab",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo '//'${var.filestorage}'.file.core.windows.net/inbound /opt/mnt/inbound cifs nofail,vers=3.0,credentials=/etc/smbcredentials/storageaccount.cred,dir_mode=0777,file_mode=0777,serverino' | sudo tee --append /etc/fstab",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo '//'${var.filestorage}'.file.core.windows.net/jams /opt/mnt/jams cifs nofail,vers=3.0,credentials=/etc/smbcredentials/storageaccount.cred,dir_mode=0777,file_mode=0777,serverino' | sudo tee --append /etc/fstab",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo '//'${var.filestorage}'.file.core.windows.net/outbound /opt/mnt/outbound cifs nofail,vers=3.0,credentials=/etc/smbcredentials/storageaccount.cred,dir_mode=0777,file_mode=0777,serverino' | sudo tee --append /etc/fstab",
    "echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S sed -i 's/ClientAliveInterval 180/ClientAliveInterval 1000/g' /etc/ssh/sshd_config",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S sed -i 's/MACs hmac-sha2-512,hmac-sha2-256/MACs hmac-sha2-512,hmac-sha2-256,hmac-sha1-96,hmac-sha1/g' /etc/ssh/sshd_config",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo 'eis soft nofile 2048' | sudo tee --append /etc/security/limits.conf",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S echo 'eis hard nofile 10240' | sudo tee --append /etc/security/limits.conf",
	"echo '${data.azurerm_key_vault_secret.os-adminpassword.value}' | sudo -S rm -rf /tmp/tomcat.service",
	"sleep 15s",
    "echo COMPLETED"
]
  connection {
    type     = "ssh"
    user     = "${var.VM_ADMIN}"
    password = "${data.azurerm_key_vault_secret.os-adminpassword.value}"
  }
 }
 provisioner "remote-exec" {
    inline = [
    "wget https://productionrespository.blob.core.windows.net/terraformbuildscripts/server.xml -P /tmp",
	"echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S yes | cp /tmp/server.xml /opt/tomcat/conf/",
	"echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S rm -rf /tmp/server.xml",
    "wget https://productionrespository.blob.core.windows.net/terraformbuildscripts/towercerts.zip -P /tmp",
	"echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S unzip /tmp/towercerts.zip -d /tmp",
	"echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S rm -rf /tmp/towercerts.zip",
	"echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S /opt/java/jre/bin/keytool -genkey -noprompt -alias tower -dname 'CN=towercloud.co.nz, OU=IT, O=TowerInsurance, L=Auckland, S=Auckland, C=NZ' -keystore /home/eis/tower.keystore -storepass '${data.azurerm_key_vault_secret.keystore.value}' -keypass '${data.azurerm_key_vault_secret.keystore.value}'",
    "echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S /opt/java/jre/bin/keytool -importkeystore -noprompt -srckeystore /tmp/server.pfx -srcstoretype pkcs12 -destkeystore /home/eis/tower.keystore -deststoretype jks -deststorepass '${data.azurerm_key_vault_secret.keystore.value}' -srcstorepass '${data.azurerm_key_vault_secret.keystore.value}'",
    "echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S /opt/java/jre/bin/keytool -import -alias intermed -keystore '\"$JAVA_HOME/jre/lib/security/cacerts\"' -trustcacerts -file /tmp/intermediate.cer -storepass 'changeit' -noprompt",
	"echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S /opt/java/jre/bin/keytool -import -alias root -keystore '\"$JAVA_HOME/jre/lib/security/cacerts\"' -trustcacerts -file /tmp/root.cer -storepass 'changeit' -noprompt",
	"echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S /opt/java/jre/bin/keytool -import -alias intermed -keystore /home/eis/tower.keystore -trustcacerts -file /tmp/intermediate.cer -deststorepass '${data.azurerm_key_vault_secret.keystore.value}' -noprompt",
	"echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S /opt/java/jre/bin/keytool -import -alias root -keystore /home/eis/tower.keystore -trustcacerts -file /tmp/root.cer -deststorepass '${data.azurerm_key_vault_secret.keystore.value}' -noprompt",
	"echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S chown -R eis:eis /home/eis/tower.keystore",
	"echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S rm -rf /tmp/intermediate.cer",
	"echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S rm -rf /tmp/root.cer",
	"echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S rm -rf /tmp/towercerts.zip",
	"echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S rm -rf /tmp/server.pfx",
	"echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S systemctl restart tomcat",
	"echo '${data.azurerm_key_vault_secret.eisserviceaccount-password.value}' | sudo -S systemctl status tomcat",
	"sleep 15s",
    "echo COMPLETED"
]
  connection {
    type     = "ssh"
    user     = "eis"
    password = "${data.azurerm_key_vault_secret.eisserviceaccount-password.value}"
  }
 }
 
 
 
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}







