# ********************** KEYVAULT SECRETS ********************** #

data "azurerm_key_vault_secret" "eisserviceaccount-password" {
  name      = "EISLocalUserPassword"
  vault_uri = "https://towerkeyvault.vault.azure.net/"
}

data "azurerm_key_vault_secret" "os-adminpassword" {
  name      = "LocalAdminPasswordOS"
  vault_uri = "https://towerkeyvault.vault.azure.net/"
}

data "azurerm_key_vault_secret" "domainJoinpassword" {
  name      = "DomainJoinPassword"
  vault_uri = "https://towerkeyvault.vault.azure.net/"
}

data "azurerm_key_vault_secret" "keystore" {
  name      = "keystore"
  vault_uri = "https://towerkeyvault.vault.azure.net/"
}