# ********************** EXTENSIONS APPLICATION ********************** #
	
resource "azurerm_virtual_machine_extension" "app" {
  name                 = "AGENTS"
  resource_group_name  = "${azurerm_resource_group.compute.name}"
  location             = "${azurerm_resource_group.compute.location}"
  virtual_machine_name = "${azurerm_virtual_machine.app.name}"
  publisher            = "Microsoft.OSTCExtensions"
  type                 = "CustomScriptForLinux"
  type_handler_version = "1.5"
  auto_upgrade_minor_version = "true"
  depends_on           = ["azurerm_virtual_machine.app"]

  settings = <<SETTINGS
    {
       "fileUris": ["https://productionrespository.blob.core.windows.net/terraformbuildscripts/linuxbasesettings.sh"], 
       "commandToExecute": "bash linuxbasesettings.sh",
	   "timestamp": 2
	   
    }
  SETTINGS
  	tags {
    "EIS" = "${var.app_tag_name}"
    }
    }
	
# ********************** EXTENSIONS APPLICATION ********************** #
	
resource "azurerm_virtual_machine_extension" "rat" {
  name                 = "AGENTS"
  resource_group_name  = "${azurerm_resource_group.compute.name}"
  location             = "${azurerm_resource_group.compute.location}"
  virtual_machine_name = "${azurerm_virtual_machine.rat.name}"
  publisher            = "Microsoft.OSTCExtensions"
  type                 = "CustomScriptForLinux"
  type_handler_version = "1.5"
  auto_upgrade_minor_version = "true"
  depends_on           = ["azurerm_virtual_machine.rat"]

  settings = <<SETTINGS
    {
       "fileUris": ["https://productionrespository.blob.core.windows.net/terraformbuildscripts/linuxbasesettings.sh"], 
       "commandToExecute": "bash linuxbasesettings.sh",
	   "timestamp": 2
	   
    }
  SETTINGS
  	tags {
    "EIS" = "${var.app_tag_name}"
    }
    }
	
	# ********************** EXTENSIONS APPLICATION ********************** #
	
resource "azurerm_virtual_machine_extension" "adm" {
  name                 = "AGENTS"
  resource_group_name  = "${azurerm_resource_group.compute.name}"
  location             = "${azurerm_resource_group.compute.location}"
  virtual_machine_name = "${azurerm_virtual_machine.adm.name}"
  publisher            = "Microsoft.OSTCExtensions"
  type                 = "CustomScriptForLinux"
  type_handler_version = "1.5"
  auto_upgrade_minor_version = "true"
  depends_on           = ["azurerm_virtual_machine.adm"]

  settings = <<SETTINGS
    {
       "fileUris": ["https://productionrespository.blob.core.windows.net/terraformbuildscripts/linuxbasesettings.sh"], 
       "commandToExecute": "bash linuxbasesettings.sh",
	   "timestamp": 2
	   
    }
  SETTINGS
  	tags {
    "EIS" = "${var.app_tag_name}"
    }
    }
	
	
	# ********************** EXTENSIONS APPLICATION ********************** #
	
resource "azurerm_virtual_machine_extension" "sol" {
  name                 = "AGENTS"
  resource_group_name  = "${azurerm_resource_group.compute.name}"
  location             = "${azurerm_resource_group.compute.location}"
  virtual_machine_name = "${azurerm_virtual_machine.sol.name}"
  publisher            = "Microsoft.OSTCExtensions"
  type                 = "CustomScriptForLinux"
  type_handler_version = "1.5"
  auto_upgrade_minor_version = "true"
  depends_on           = ["azurerm_virtual_machine.sol"]

  settings = <<SETTINGS
    {
       "fileUris": ["https://productionrespository.blob.core.windows.net/terraformbuildscripts/linuxbasesettings.sh"], 
       "commandToExecute": "bash linuxbasesettings.sh",
	   "timestamp": 2
	   
    }
  SETTINGS
  	tags {
    "EIS" = "${var.app_tag_name}"
    }
    }
	
	# ********************** EXTENSIONS APPLICATION ********************** #
	
resource "azurerm_virtual_machine_extension" "dxp" {
  name                 = "AGENTS"
  resource_group_name  = "${azurerm_resource_group.compute.name}"
  location             = "${azurerm_resource_group.compute.location}"
  virtual_machine_name = "${azurerm_virtual_machine.dxp.name}"
  publisher            = "Microsoft.OSTCExtensions"
  type                 = "CustomScriptForLinux"
  type_handler_version = "1.5"
  auto_upgrade_minor_version = "true"
  depends_on           = ["azurerm_virtual_machine.dxp"]

  settings = <<SETTINGS
    {
       "fileUris": ["https://productionrespository.blob.core.windows.net/terraformbuildscripts/linuxbasesettings.sh"], 
       "commandToExecute": "bash linuxbasesettings.sh",
	   "timestamp": 2
	   
    }
  SETTINGS
  	tags {
    "EIS" = "${var.app_tag_name}"
    }
    }

	

