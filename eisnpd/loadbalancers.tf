# **********************  LOAD BALANCERS ********************** #

resource "azurerm_lb" "adm" {
  name                      = "${var.environment}ADM-LB"
  resource_group_name       = "${azurerm_resource_group.network.name}"
  location                  = "${azurerm_resource_group.network.location}"
  
  frontend_ip_configuration {
  name                      = "${var.environment}ADM-FP"
  subnet_id                 = "${azurerm_subnet.lb.id}"
  private_ip_address_allocation = "static"
  private_ip_address        = "${var.admlb_ip}"
  }
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}

resource "azurerm_lb_backend_address_pool" "adm" {
  resource_group_name       = "${azurerm_resource_group.network.name}"
  loadbalancer_id           = "${azurerm_lb.adm.id}"
  name                      = "${var.environment}ADM-BP"
}

resource "azurerm_lb_probe" "adm01" {
  resource_group_name       = "${azurerm_resource_group.network.name}"
  loadbalancer_id           = "${azurerm_lb.adm.id}"
  name                      = "${var.environment}ADM-PROBE01"
  protocol                  = "tcp"
  port                      = 8080
  interval_in_seconds       = 5
  number_of_probes          = 2
}

resource "azurerm_lb_probe" "adm02" {
  resource_group_name       = "${azurerm_resource_group.network.name}"
  loadbalancer_id           = "${azurerm_lb.adm.id}"
  name                      = "${var.environment}ADM-PROBE02"
  protocol                  = "tcp"
  port                      = 8443
  interval_in_seconds       = 5
  number_of_probes          = 2
}

resource "azurerm_lb_rule" "adm01" {
  resource_group_name            = "${azurerm_resource_group.network.name}"
  loadbalancer_id                = "${azurerm_lb.adm.id}"
  name                           = "${var.environment}ADM-RULE01"
  protocol                       = "tcp"
  frontend_port                  = 80
  backend_port                   = 8080
  frontend_ip_configuration_name = "${lookup(azurerm_lb.adm.frontend_ip_configuration[0],"name")}"
  enable_floating_ip             = false
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.adm.id}"
  idle_timeout_in_minutes        = 5
  probe_id                       = "${azurerm_lb_probe.adm01.id}"
  load_distribution              = "SourceIP"
  depends_on                     = ["azurerm_lb_probe.adm01"]
}

resource "azurerm_lb_rule" "adm02" {
  resource_group_name            = "${azurerm_resource_group.network.name}"
  loadbalancer_id                = "${azurerm_lb.adm.id}"
  name                           = "${var.environment}ADM-RULE02"
  protocol                       = "tcp"
  frontend_port                  = 443
  backend_port                   = 8443
  frontend_ip_configuration_name = "${lookup(azurerm_lb.adm.frontend_ip_configuration[0],"name")}"
  enable_floating_ip             = false
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.adm.id}"
  idle_timeout_in_minutes        = 5
  probe_id                       = "${azurerm_lb_probe.adm02.id}"
  load_distribution              = "SourceIP"
  depends_on                     = ["azurerm_lb_probe.adm02"]
}


# **********************  LOAD BALANCERS ********************** #

resource "azurerm_lb" "app" {
  name                      = "${var.environment}APP-LB"
  resource_group_name       = "${azurerm_resource_group.network.name}"
  location                  = "${azurerm_resource_group.network.location}"
  depends_on                = ["azurerm_virtual_network.vnet", "azurerm_subnet.lb"]
  
  frontend_ip_configuration {
  name                      = "${var.environment}APP-FP"
  subnet_id                 = "${azurerm_subnet.lb.id}"
  private_ip_address_allocation = "static"
  private_ip_address        = "${var.applb_ip}"
  }
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}

resource "azurerm_lb_backend_address_pool" "app" {
  resource_group_name       = "${azurerm_resource_group.network.name}"
  loadbalancer_id           = "${azurerm_lb.app.id}"
  name                      = "${var.environment}APP-BP"
}

resource "azurerm_lb_probe" "app01" {
  resource_group_name       = "${azurerm_resource_group.network.name}"
  loadbalancer_id           = "${azurerm_lb.app.id}"
  name                      = "${var.environment}APP-PROBE01"
  protocol                  = "tcp"
  port                      = 8080
  interval_in_seconds       = 5
  number_of_probes          = 2
}

resource "azurerm_lb_rule" "app01" {
  resource_group_name            = "${azurerm_resource_group.network.name}"
  loadbalancer_id                = "${azurerm_lb.app.id}"
  name                           = "${var.environment}APP-RULE01"
  protocol                       = "tcp"
  frontend_port                  = 80
  backend_port                   = 8080
  frontend_ip_configuration_name = "${lookup(azurerm_lb.app.frontend_ip_configuration[0],"name")}"
  enable_floating_ip             = false
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.app.id}"
  idle_timeout_in_minutes        = 5
  probe_id                       = "${azurerm_lb_probe.app01.id}"
  load_distribution              = "SourceIP"
  depends_on                     = ["azurerm_lb_probe.app01"]
}

resource "azurerm_lb_probe" "app02" {
  resource_group_name       = "${azurerm_resource_group.network.name}"
  loadbalancer_id           = "${azurerm_lb.app.id}"
  name                      = "${var.environment}APP-PROBE02"
  protocol                  = "tcp"
  port                      = 8443
  interval_in_seconds       = 5
  number_of_probes          = 2
}

resource "azurerm_lb_rule" "app02" {
  resource_group_name            = "${azurerm_resource_group.network.name}"
  loadbalancer_id                = "${azurerm_lb.app.id}"
  name                           = "${var.environment}APP-RULE02"
  protocol                       = "tcp"
  frontend_port                  = 443
  backend_port                   = 8443
  frontend_ip_configuration_name = "${lookup(azurerm_lb.app.frontend_ip_configuration[0],"name")}"
  enable_floating_ip             = false
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.app.id}"
  idle_timeout_in_minutes        = 5
  probe_id                       = "${azurerm_lb_probe.app02.id}"
  load_distribution              = "SourceIP"
  depends_on                     = ["azurerm_lb_probe.app02"]
}

# **********************  LOAD BALANCERS ********************** #

resource "azurerm_lb" "rat" {
  name                      = "${var.environment}RAT-LB"
  resource_group_name       = "${azurerm_resource_group.network.name}"
  location                  = "${azurerm_resource_group.network.location}"
  depends_on                = ["azurerm_virtual_network.vnet", "azurerm_subnet.lb"]
  
  frontend_ip_configuration {
  name                      = "${var.environment}RAT-FP"
  subnet_id                 = "${azurerm_subnet.lb.id}"
  private_ip_address_allocation = "static"
  private_ip_address        = "${var.ratlb_ip}"
  }
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}

resource "azurerm_lb_backend_address_pool" "rat" {
  resource_group_name       = "${azurerm_resource_group.network.name}"
  loadbalancer_id           = "${azurerm_lb.rat.id}"
  name                      = "${var.environment}RAT-BP"
}

resource "azurerm_lb_probe" "rat01" {
  resource_group_name       = "${azurerm_resource_group.network.name}"
  loadbalancer_id           = "${azurerm_lb.rat.id}"
  name                      = "${var.environment}RAT-PROBE01"
  protocol                  = "tcp"
  port                      = 8080
  interval_in_seconds       = 5
  number_of_probes          = 2
}

resource "azurerm_lb_rule" "rat01" {
  resource_group_name            = "${azurerm_resource_group.network.name}"
  loadbalancer_id                = "${azurerm_lb.rat.id}"
  name                           = "${var.environment}RAT-RULE01"
  protocol                       = "tcp"
  frontend_port                  = 80
  backend_port                   = 8080
  frontend_ip_configuration_name = "${lookup(azurerm_lb.rat.frontend_ip_configuration[0],"name")}"
  enable_floating_ip             = false
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.rat.id}"
  idle_timeout_in_minutes        = 5
  probe_id                       = "${azurerm_lb_probe.rat01.id}"
  load_distribution              = "SourceIP"
  depends_on                     = ["azurerm_lb_probe.rat01"]
}

resource "azurerm_lb_probe" "rat02" {
  resource_group_name       = "${azurerm_resource_group.network.name}"
  loadbalancer_id           = "${azurerm_lb.rat.id}"
  name                      = "${var.environment}RAT-PROBE02"
  protocol                  = "tcp"
  port                      = 8443
  interval_in_seconds       = 5
  number_of_probes          = 2
}

resource "azurerm_lb_rule" "rat02" {
  resource_group_name            = "${azurerm_resource_group.network.name}"
  loadbalancer_id                = "${azurerm_lb.rat.id}"
  name                           = "${var.environment}RAT-RULE02"
  protocol                       = "tcp"
  frontend_port                  = 443
  backend_port                   = 8443
  frontend_ip_configuration_name = "${lookup(azurerm_lb.rat.frontend_ip_configuration[0],"name")}"
  enable_floating_ip             = false
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.rat.id}"
  idle_timeout_in_minutes        = 5
  probe_id                       = "${azurerm_lb_probe.rat02.id}"
  load_distribution              = "SourceIP"
  depends_on                     = ["azurerm_lb_probe.rat02"]
}

# **********************  LOAD BALANCERS ********************** #

resource "azurerm_lb" "sol" {
  name                      = "${var.environment}SOL-LB"
  resource_group_name       = "${azurerm_resource_group.network.name}"
  location                  = "${azurerm_resource_group.network.location}"
  depends_on                = ["azurerm_virtual_network.vnet", "azurerm_subnet.lb"]
  
  frontend_ip_configuration {
  name                      = "${var.environment}SOL-FP"
  subnet_id                 = "${azurerm_subnet.lb.id}"
  private_ip_address_allocation = "static"
  private_ip_address        = "${var.sollb_ip}"
  }
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}

resource "azurerm_lb_backend_address_pool" "sol" {
  resource_group_name       = "${azurerm_resource_group.network.name}"
  loadbalancer_id           = "${azurerm_lb.sol.id}"
  name                      = "${var.environment}SOL-BP"
}

resource "azurerm_lb_probe" "sol01" {
  resource_group_name       = "${azurerm_resource_group.network.name}"
  loadbalancer_id           = "${azurerm_lb.sol.id}"
  name                      = "${var.environment}SOL-PROBE01"
  protocol                  = "tcp"
  port                      = 8984
  interval_in_seconds       = 5
  number_of_probes          = 2
}

resource "azurerm_lb_rule" "sol01" {
  resource_group_name            = "${azurerm_resource_group.network.name}"
  loadbalancer_id                = "${azurerm_lb.sol.id}"
  name                           = "${var.environment}SOL-RULE01"
  protocol                       = "tcp"
  frontend_port                  = 80
  backend_port                   = 8984
  frontend_ip_configuration_name = "${lookup(azurerm_lb.sol.frontend_ip_configuration[0],"name")}"
  enable_floating_ip             = false
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.sol.id}"
  idle_timeout_in_minutes        = 5
  probe_id                       = "${azurerm_lb_probe.sol01.id}"
  load_distribution              = "SourceIP"
  depends_on                     = ["azurerm_lb_probe.sol01"]
}


# **********************  LOAD BALANCERS ********************** #

resource "azurerm_lb" "dxp" {
  name                      = "${var.environment}DXP-LB"
  resource_group_name       = "${azurerm_resource_group.network.name}"
  location                  = "${azurerm_resource_group.network.location}"
  depends_on                = ["azurerm_virtual_network.vnet", "azurerm_subnet.lb"]
  
  frontend_ip_configuration {
  name                      = "${var.environment}DXP-FP"
  subnet_id                 = "${azurerm_subnet.lb.id}"
  private_ip_address_allocation = "static"
  private_ip_address        = "${var.dxplb_ip}"
  }
  
  tags {
  "EIS" = "${var.app_tag_name}"
  }
}

resource "azurerm_lb_backend_address_pool" "dxp" {
  resource_group_name       = "${azurerm_resource_group.network.name}"
  loadbalancer_id           = "${azurerm_lb.dxp.id}"
  name                      = "${var.environment}DXP-BP"
}

resource "azurerm_lb_probe" "dxp01" {
  resource_group_name       = "${azurerm_resource_group.network.name}"
  loadbalancer_id           = "${azurerm_lb.dxp.id}"
  name                      = "${var.environment}DXP-PROBE01"
  protocol                  = "tcp"
  port                      = 9443
  interval_in_seconds       = 5
  number_of_probes          = 2
}


resource "azurerm_lb_rule" "dxp01" {
  resource_group_name            = "${azurerm_resource_group.network.name}"
  loadbalancer_id                = "${azurerm_lb.dxp.id}"
  name                           = "${var.environment}DXP-RULE01"
  protocol                       = "tcp"
  frontend_port                  = 443
  backend_port                   = 9443
  frontend_ip_configuration_name = "${lookup(azurerm_lb.dxp.frontend_ip_configuration[0],"name")}"
  enable_floating_ip             = false
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.dxp.id}"
  idle_timeout_in_minutes        = 5
  probe_id                       = "${azurerm_lb_probe.dxp01.id}"
  load_distribution              = "SourceIP"
  depends_on                     = ["azurerm_lb_probe.dxp01"]
}

