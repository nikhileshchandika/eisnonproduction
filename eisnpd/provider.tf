# **********************  PROVIDERS ********************** #


provider "azurerm" {
subscription_id="${var.AZURE_SUBSCRIPTION_ID}"
client_id="${var.AZURE_CLIENT_ID}"
client_secret="${var.AZURE_CLIENT_SECRET}"
tenant_id="${var.AZURE_TENANT_ID}"
}

provider "azurerm" {
alias = "gateway"
subscription_id="${var.AZURE_SUBSCRIPTION_TOWERA_ID}"
client_id="${var.AZURE_CLIENT_ID}"
client_secret="${var.AZURE_CLIENT_SECRET}"
tenant_id="${var.AZURE_TENANT_ID}"
}
